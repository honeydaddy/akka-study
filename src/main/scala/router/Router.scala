package router

import akka.actor.typed.receptionist.{Receptionist, ServiceKey}
import akka.actor.typed.{ActorSystem, Behavior, DispatcherSelector, SupervisorStrategy}
import akka.actor.typed.scaladsl.{Behaviors, Routers}

import java.lang.Thread.sleep
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt

/*
  - 메시지 병렬 처리를 위해 동일 유형의 메시지를 액터 세트에 배포하는 것이 유용하다.
  - 단일 액터는 한번에 하나의 메시지만 처리한다.
  - 자식 액터가 중지되문 poll router는 해당 자식을 제거한다. 마지막 자식이 중지하면 라우터 자체가 중지된다. (복원력이 있는 라우터를 만들려면 감시 필요)
  - Pool Router
    : Router.pool
    : poolwithBroadCastPredicate
  - Group Router
    : ServiceKey를 생성하고 Receptionist 사용.
  - Routing 전략
    : round-robin
    : random
    : hashing
      // val ourter = spawn(Router.group(Proxy.RegisteringKey).withConsistentHasingRouting(10, Proxy.mapping)
  ? Dispatcher?? 구성??
 */

object Router {

  class DoBroadcastLog(text: String) extends Worker.DoLog(text)
  object DoBroadcastLog {
    def apply(text: String) = new DoBroadcastLog(text)
  }

  object Worker {
    sealed trait Command
    case class DoLog(text: String) extends Command

    def apply(): Behavior[Command] = Behaviors.setup { context =>
      println(s"${context.self.toString} Starting Worker")

      Behaviors.receiveMessage {
        case DoLog(text) =>
          println(s"${context.self.toString} Got message $text")
          Behaviors.same
      }
    }
  }

  object PoolRouter {
    def apply(): Behavior[Unit] = Behaviors.setup[Unit] { context =>
      val pool = Routers.pool(poolSize = 4) {
        Behaviors.supervise(Worker()).onFailure[Exception](SupervisorStrategy.restart)
      }
      val router = context.spawn(pool, "woker-pool")

      (0 to 4).foreach { n =>
        router ! Worker.DoLog(s"msg $n")
      }

      sleep(1000)

      val poolWithBroadcast = pool.withBroadcastPredicate(_.isInstanceOf[DoBroadcastLog])
      val routeWithBroadcate = context.spawn(poolWithBroadcast, "pool-with-broadcast")

      (0 to 4).foreach { n =>
        routeWithBroadcate ! DoBroadcastLog(s"msg broadcast $n")
      }
      Behaviors.empty
    }
  }

  val serviceKey = ServiceKey[Worker.Command]("log-worker")
  object GroupRouter {
    def apply(): Behavior[Unit] = Behaviors.setup[Unit] { ctx =>
      val worker = ctx.spawn(Worker(), "worker")
      ctx.system.receptionist ! Receptionist.Register(serviceKey, worker)

      val worker2 = ctx.spawn(Worker(), "worker2")
      ctx.system.receptionist ! Receptionist.Register(serviceKey, worker2)

      val group = Routers.group(serviceKey)
      val router = ctx.spawn(group, "worker-group")

      (0 to 10).foreach { n =>
        router ! Worker.DoLog(s"msg $n")
      }
      Behaviors.empty
    }
  }

  def main(args: Array[String]): Unit = {
    //val system = ActorSystem(PoolRouter(), "root-behavior")
    val system = ActorSystem(GroupRouter(), "root-behavior")

    implicit val ec: ExecutionContext = system.dispatchers.lookup(DispatcherSelector.default)
    system.scheduler.scheduleOnce(1.second, () => system.terminate())
  }

}
