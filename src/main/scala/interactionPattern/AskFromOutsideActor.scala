package interactionPattern

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, DispatcherSelector, Dispatchers, Scheduler}
import akka.actor.typed.scaladsl.AskPattern._
import akka.util.Timeout

import java.lang.Thread.sleep
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.Failure
import scala.util.Success

/*
  - 액터시스템 외부에서 요청을 통한 요청/응답이 ask를 통해 가능하다.
  - Future[Response]로 Succss 또는 Failure로 응답
  - 암시적 변환 선언 필요하다. ( ActorRef by akka.actor.typed.scaladsl.AskPattern._ / Timeout / ActorSystem )

  - Use Case
    : 액터시스템 외부에서 액터로 쿼리할 경우

  - Problems
    : Future로 리턴되는 콜백이 다른 스레드에서 실행되기 때문에 실수로 닫히거나 변경 가능한 상태에서 unsafe하다.
    : 하나의 ask에서 단일 응답만 가능하다.
    : Timeout이 발생할 때 수신 액터는 이를 모르고 처리 진행/완료
 */

object AskFromOutsideActor {
  object CookieFabric {
    sealed trait Command
    case class GiveMeCookies(count: Int, replyTo: ActorRef[Reply]) extends Command

    sealed trait Reply
    case class Cookies(count: Int) extends Reply
    // 메시지 프로토콜 내 명확한 에러 검증
    case class InvalidRequest(reason: String) extends Reply

    def apply(): Behaviors.Receive[CookieFabric.GiveMeCookies] =
      Behaviors.receiveMessage { message =>
        // GiveMeCookies는 두 가지 응답 유형을 갖는다.
        // 응답을 받는 쪽에서 적절한 처리 필요
        if (message.count >= 5)
            message.replyTo ! InvalidRequest("Too many cookies.")
        else
            message.replyTo ! Cookies(message.count)
        Behaviors.same
      }
  }

  def main(args: Array[String]): Unit = {
    import CookieFabric._

    // implicit ActorSystem in scope
    implicit val system = ActorSystem(CookieFabric(), "main-app")
    // Timeout
    implicit val timeout: Timeout = 1.seconds

    /*
    implicit val ec: ExecutionContext = system.dispatchers.lookup(DispatcherSelector.default)
    system.scheduler.scheduleOnce(5.second, () => system.terminate())
    */
    // the response callback will be executed on this execution context
    implicit val ec: ExecutionContext = system.executionContext
    system.scheduler.scheduleOnce(5.second, () => system.terminate())

    val result:Future[Reply] = system.ask(ref => GiveMeCookies(4, ref))

    result.onComplete {
      case Success(Cookies(count)) => println(s"Yay, $count cookies!")
      case Success(InvalidRequest(reason)) => println(s"No cookies for me. $reason")
      case Failure(ex) => println(s"Boo! didn't get cookies: ${ex.getMessage}")
    }
  }
}
