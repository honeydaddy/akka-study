package interactionPattern

import akka.NotUsed
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import akka.util.Timeout
import akka.actor.typed.scaladsl.AskPattern._

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.Failure
import scala.util.Success

/*
  - 떄때로 어떤 응답은 다른 액터들을 생성하여 응답을 수집하고 전달하는 경우도 있다.
  - 위와 같은 경우 'session' 자식 actor에 위임하는 것이 좋다.
  - 자식 actor는 재시도, 시간초과실패, 진행검사등을 구현할 수 있다.

  - Use Case
    : 요청에 응답하기 위해 다른 액터들의 결과 집계가 필요한 경우
    : 최소 1회 전당을 위한 승인 및 재시도가 필요한 경우

  - Problems
    : 세션 액터가 멈추지 않는 시나리오를 놓치기 쉽다.
    : 각 자식은 다른 자식 및 부모 액터와 동시에 실행할 수 있기 때문에 복잡도가 증가한다.
 */

object PerSessionChildActor {
  // dummy data types just for this sample
  case class Keys()
  case class Wallet()

  object KeyCabinet {
    case class GetKeys(whoseKeys: String, replyTo: ActorRef[Keys])

    def apply(): Behavior[GetKeys] =
      Behaviors.receiveMessage {
        case GetKeys(_, replyTo) =>
          println("get keys")
          replyTo ! Keys()
          Behaviors.same
      }
  }

  object Drawer {
    case class GetWallet(whoseWallet: String, replyTo: ActorRef[Wallet])

    def apply(): Behavior[GetWallet] =
      Behaviors.receiveMessage {
        case GetWallet(_, replyTo) =>
          println("get wallet")
          replyTo ! Wallet()
          Behaviors.same
      }

  }

  object Home {
    sealed trait Command
    case class LeaveHome(who: String, replyTo: ActorRef[ReadyToLeaveHome]) extends Command
    case class ReadyToLeaveHome(who: String, keys: Keys, wallet: Wallet)

    def apply(): Behavior[Command] = {
      Behaviors.setup[Command] { context =>
        val keyCabinet: ActorRef[KeyCabinet.GetKeys] = context.spawn(KeyCabinet(), "key-cabinet")
        val drawer: ActorRef[Drawer.GetWallet] = context.spawn(Drawer(), "drawer")

        Behaviors.receiveMessage[Command] {
          case LeaveHome(who, replyTo) =>
            context.spawn(prepareToLeaveHome(who, replyTo, keyCabinet, drawer), s"leaving-$who")
            Behaviors.same
        }
      }
    }

    def prepareToLeaveHome(
      whoIsLeaving: String,
      replyTo: ActorRef[Home.ReadyToLeaveHome],
      keyCabinet: ActorRef[PerSessionChildActor.KeyCabinet.GetKeys],
      drawer: ActorRef[PerSessionChildActor.Drawer.GetWallet]): Behavior[NotUsed] = {

      Behaviors.setup[AnyRef] { context =>
        var wallet: Option[Wallet] = None
        var keys: Option[Keys] = None

        keyCabinet ! KeyCabinet.GetKeys(whoIsLeaving, context.self.narrow[Keys])
        drawer ! Drawer.GetWallet(whoIsLeaving, context.self.narrow[Wallet])

        def nextBehavior(): Behavior[AnyRef] =
          (keys, wallet) match {
            case (Some(w), Some(k)) =>
              replyTo ! ReadyToLeaveHome(whoIsLeaving, w, k)
              Behaviors.stopped

            case _ =>
              Behaviors.same
          }

        Behaviors.receiveMessage {
          case w: Wallet =>
            wallet = Some(w)
            nextBehavior()
          case k: Keys =>
            keys = Some(k)
            nextBehavior()
          case _ =>
            Behaviors.unhandled
        }
      }.narrow[NotUsed]
    }
  }

  def main(args: Array[String]): Unit = {
    import Home._
    implicit val system:ActorSystem[Command] = ActorSystem(Home(), "home")
    implicit val timeout: Timeout = 1.seconds
    implicit val ec: ExecutionContext = system.executionContext
    system.scheduler.scheduleOnce(5.second, () => system.terminate())

    val result:Future[ReadyToLeaveHome] = system.ask(ref => Home.LeaveHome("sbkim", ref))

    result.onComplete {
      case Success(ReadyToLeaveHome(who, keys, wallets)) => println(s"$who - $keys - $wallets")
      case Failure(ex) => println(s"Boo! didn't leave home: ${ex.getMessage}")
    }
  }

}
