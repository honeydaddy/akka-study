package interactionPattern

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, DispatcherSelector, Dispatchers}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

/*
  - 전송 액터는 다른 액터의 응답 메시지를 지원하지 않으며, 지원해서는 안된다.
  - 이러한 경우 올바른 타입(응답 메시지를 전송하는 다른 액터)의 ActorRef를 제공하고, 전송 액터가 처리할 수 있는 응답 유형으로 적용(adapter)한다.

  - 하나의 Message는 하나의 MessageAdapter를 가질 수 있다.
  - MessageAdpter는 수신 액터와 동일한 생명주기를 갖는다.

  - Use Case
    : 다른 액터 프로토콜 간 변환이 필요한 경우
    : 많은 응답 메시지를 다시 보낼 액터를 구독하는 경우

  - Problems
    : 메시지 요청이 전달/처리 되지 않았음을 감지하기 어렵다.
    : 응답 메시지 유형 당 하나의 adapter만 적용 가능하다.
    : 메시지 내에 컨텍스트 제공하는 방법이 포함되지 않는 한 컨텍스트 연결이 어렵다. (응답으로도 전송되는 요청 ID가 필요하다???)
 */

/*
  - Sample Code Case
    : Customer <-> Checkout <-> ShoppingCart
    : 위 관계에서 Checkout Actor는 두 가지 유형의 메시지를 처리해야 함
      > Checkout.Request
      > ShoppingCart.Response
    : typed actor에서는 Any를 사용하지 않는 한 위 메시지들을 통합할 수 없다. (Any를 사용하는 것은 unypted actor로 돌아가는 꼴)
    : 위 문제를 해결하기 위해 MessageAdapter를 사용
      > ShoppingCart.Response를 Checkout.Request로 래핑하고 messageAdapter 등록
 */

object MessageAdaptation {

  object StoreDomain {
    case class Product(name: String, price: Double)
  }

  object ShoppingCart {
    import StoreDomain._

    sealed trait Request
    case class GetCurrentCart(cartId: String, replyTo: ActorRef[Response]) extends Request

    sealed trait Response
    case class CurrentCart(cartId: String, imtes: List[Product]) extends Response

    val db: Map[String, List[Product]] = Map {
      "123-abc-456" -> List(Product("iPhone", 7000), Product("selfie stick", 30))
    }

    def apply(): Behavior[Request] = Behaviors.receiveMessage{
      case GetCurrentCart(cartId, replyTo) =>
        replyTo ! CurrentCart(cartId, db(cartId))
        Behaviors.same
    }
  }

  object Checkout {
    import ShoppingCart._

    sealed trait Request
    final case class InspectSummary(cartId: String, replyTo: ActorRef[Response]) extends Request

    private final case class WrappedSCRResponse(response: ShoppingCart.Response) extends Request

    sealed trait Response
    final case class Summary(cartId: String, amount: Double) extends Response

    def apply(shoppingCart: ActorRef[ShoppingCart.Request]): Behavior[Request] =
      Behaviors.setup[Request] { context =>

        // MessageAdapter
        val responseMapper: ActorRef[ShoppingCart.Response] =
          context.messageAdapter(rsp => WrappedSCRResponse(rsp))

        def handlingCheckout(checkoutsInProgress: Map[String, ActorRef[Response]]): Behavior[Request] = {
          Behaviors.receiveMessage[Request] {
            case InspectSummary(cartId, replyTo) =>
              shoppingCart ! ShoppingCart.GetCurrentCart(cartId, responseMapper)
              handlingCheckout(checkoutsInProgress + (cartId -> replyTo))

            case WrappedSCRResponse(resp) =>
              resp match {
                case CurrentCart(cartId, items) =>
                  val summary = Summary(cartId, items.map(_.price).sum)
                  val customer = checkoutsInProgress(cartId)
                  customer ! summary
                  Behaviors.same
              }
          }
        }

        handlingCheckout(checkoutsInProgress = Map())
      }
  }

  def main(args: Array[String]): Unit = {
    import Checkout._

    val rootBehavior: Behavior[Any] = Behaviors.setup { context =>
      val shoppingCart = context.spawn(ShoppingCart(), "shopping-cart")

      val customer = context.spawn(Behaviors.receiveMessage[Response] {
        case Summary(_, amount) =>
          print(s"Total to apy: $amount - pay by card below.")
          Behaviors.same
      }, "customer")

      val checkout = context.spawn(Checkout(shoppingCart), "checkout")

      checkout ! InspectSummary("123-abc-456", customer)

      Behaviors.empty
    }

    val system = ActorSystem(rootBehavior, "main-app")
    implicit val ec: ExecutionContext = system.dispatchers.lookup(DispatcherSelector.default)
    system.scheduler.scheduleOnce(1.second, () => system.terminate())
  }
}