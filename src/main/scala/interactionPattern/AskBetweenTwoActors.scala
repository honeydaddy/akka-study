package interactionPattern

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, DispatcherSelector, Dispatchers}
import akka.util.Timeout

import java.lang.Thread.sleep
import scala.concurrent.{ExecutionContext, TimeoutException}
import scala.concurrent.duration._
import scala.util.Failure
import scala.util.Success

/*
  - 송/수신 액터가 1:1 관계일 경우 Actor Context의 ask를 사용해 통신할 수 있다.

  - Use Case
    : 단일 응답 쿼리
    : 액터가 계속 진행하기 전 메시지가 처리되었음을 알아야 할 경우
    : 적시 응답이 생성되지 않은 경우 재전송 해야할 경우
    : 미결 요청을 추적하고, 수신 엑터에 부담을 주지 않아야 할 경우
    : 송/수신에 context를 연결해야하지만 프로토콜이 이를 지원하지 않을 경우

  - Problems
    : 하나의 ask에서 단일 응답만 가능하다.
    : Timeout이 발생할 때 수신 액터는 이를 모르고 처리 진행/완료
    : ?
 */

object AskBetweenTwoActors {

  object Hal {
    sealed trait Command
    case class OpenThePodBayDoorsPlease(replyTo: ActorRef[Response]) extends Command
    case class Response(message: String)

    def apply(): Behaviors.Receive[Command] =
      Behaviors.receiveMessage[Command] {
        case OpenThePodBayDoorsPlease(replyTo) =>
          sleep(3100)
          replyTo ! Response("I'm sorry, Dave, I'm afraid I can't do that.")
          Behaviors.same
      }
  }

  object Dave {
    sealed trait Command
    // 액터 내부에서 사용할 프로토콜의 일부
    private case class AdaptedResponse(message: String) extends Command

    def apply(hal: ActorRef[Hal.Command]): Behavior[Command] =
      Behaviors.setup[Command] { context =>
        // ask 사용을 위해 timeout이 필요하며, timeout이 발생할 경우 TimeoutException 발생한다.
        implicit val timeout: Timeout = 3.seconds

        // 두번째 파라미너틑 'ActorRef[T] => message' 타입의 함수를 인자로 받는다.
        // OpenThePodBayDoorsPlease는 팩토리 apply 메서드를 갖는 케이스 클래스로 아래 구문으로 대체 가능하다.
        // 'ref => OpenThePodBayDoorsPlease(ref)
        context.ask(hal, Hal.OpenThePodBayDoorsPlease) {
          case Success(Hal.Response(message)) => AdaptedResponse(message)
          case Failure(_) => AdaptedResponse("Request failed")
        }

        // requst context와 송/수신을 연결할 수 있다. (immutable state에서..)
        val requestId = 1
        context.ask(hal, Hal.OpenThePodBayDoorsPlease) {
          case Success(Hal.Response(message)) => AdaptedResponse(s"$requestId: $message")
          case Failure(_)                     => AdaptedResponse(s"$requestId: Request failed")
        }

        Behaviors.receiveMessage {
          // Adapted message(AdptedResponse)는 액터에게 보낸 다른 메시지처럼 처리된다.
          case AdaptedResponse(message) =>
            println(s"Got response from hal : $message")
            Behaviors.same
        }
      }
  }

  def main(args: Array[String]): Unit = {
    val rootBehavior: Behavior[Any] = Behaviors.setup { context =>
      val hal = context.spawn(Hal(), "hal")
      context.spawn(Dave(hal), name = "dave")
      Behaviors.empty
    }

    val system = ActorSystem(rootBehavior, "main-app")
    implicit val ec: ExecutionContext = system.dispatchers.lookup(DispatcherSelector.default)
    system.scheduler.scheduleOnce(5.second, () => system.terminate())
  }
}
