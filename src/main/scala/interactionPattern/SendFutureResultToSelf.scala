package interactionPattern

import akka.Done
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, DispatcherSelector, Dispatchers}
import akka.pattern.StatusReply
import akka.util.Timeout

import java.lang.Thread.sleep
import scala.concurrent.{ExecutionContext, Future, TimeoutException}
import scala.concurrent.duration._
import scala.util.Failure
import scala.util.Success

/*
  - Future를 반환하는 API를 사용할 경우 완료 후 액터에서 응답값을 사용하는 것이 일반적이다.
  - onComplete on th Future / not thread-safe / 외부 스레드에서 내부 상태를 접근

  - Use Case
    : 데이터베이스나 외부 서비스와 같은 future를 리턴하는 API에 접근할 경우
    : Future가 완료된 후 처리가 지속되어야 할 경우
    : 원래 요청의 컨텍스트를 유지하고 완료 후 이를 사용할 경우

  - Problems
    : 결과에 대한 래퍼 메시지를 추가하는 표준 ??
 */

object SendFutureResultToSelf {

  trait CustomerDataAccess {
    def update(value: Customer): Future[Done]
  }

  final case class Customer(id: String, version: Long, name: String, address: String)

  object CustomerRepository {
    sealed trait Command
    final case class Update(value: Customer, replyTo: ActorRef[UpdateResult]) extends Command

    sealed trait UpdateResult
    final case class UpdateSuccess(id: String) extends UpdateResult
    final case class UpdateFailure(id: String, reason: String) extends UpdateResult

    private final case class WrappedUpdateResult(result: UpdateResult, replyTo: ActorRef[UpdateResult]) extends Command

    private val MaxOperationsInProgress = 10

    def apply(dataAccess: CustomerDataAccess): Behavior[Command] = {
      next(dataAccess, operationsInProgress = 0)
    }

    private def next(dataAccess: CustomerDataAccess, operationsInProgress: Int): Behavior[Command] = {
      Behaviors.receive { (context, command) =>
        command match {
          case Update(value, replyTo) =>
            if (operationsInProgress == MaxOperationsInProgress) {
              replyTo ! UpdateFailure(value.id, s"MaxOperationsInProgress concurrent operations supported")
              Behaviors.same
            } else {
              val futureResult = dataAccess.update(value)
              // future 결과를 대기? Actor 자신에게 전송?
              context.pipeToSelf(futureResult) {
                case Success(_) => WrappedUpdateResult(UpdateSuccess(value.id), replyTo)
                case Failure(e) => WrappedUpdateResult(UpdateFailure(value.id, e.getMessage), replyTo)
              }
              next(dataAccess, operationsInProgress + 1)
            }
          case WrappedUpdateResult(result, replyTo) =>
            // send result to original requestor
            replyTo ! result
            next(dataAccess, operationsInProgress - 1)
        }
      }
    }
  }


}
