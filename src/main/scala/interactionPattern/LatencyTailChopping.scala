package interactionPattern

import scala.concurrent.duration.FiniteDuration
import scala.reflect.ClassTag

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors

/*
  - GeneralPurposeResponseAgg의 변형 패턴
  - 여러 대상 Actor가 동일한 작업을 수행할 수 있고 때때로 예상보다 더 느리게 응답할 수 있는 상황에서 지연시간을 줄이는 것.
  - 동일한 작업 요청을 다른 Actor에 전송해 응답시간을 단축 ( 여러 Actor가 동시에 과부하 상태일 가능성이 낮기 때문 )

  - Use Case
    : 높은 지연 비율을 줄이고, 지연 시간 변화가 중요한 경우?
    : 작업이 두 번 이상 수행되어도 무방한 경우?

  - Problems
    : 더 많은 요청이 생김 ( 두 번 이상 수행될 수 있음 )
    : 작업이 멱등성이 아닌 경우, 반드시 한번만 수행되어야할 경우 사용할 수 없음.
    : 그 외 GeneralPurposeResponseAgg와 동일
 */

object LatencyTailChopping {
  object TailChopping {
    sealed trait Command
    private case object RequestTimeout extends Command
    private case object FinalTimeout extends Command
    private case class WrappedReply[R](reply: R) extends Command

    def apply[Reply: ClassTag](
      sendRequest: (Int, ActorRef[Reply]) => Boolean,
      nextRequestAfter: FiniteDuration,
      replyTo: ActorRef[Reply],
      finalTimeout: FiniteDuration,
      timeoutReply: Reply
    ): Behavior[Command] = {
      Behaviors.setup { context =>
        Behaviors.withTimers { timers =>
          val replyAdapter = context.messageAdapter[Reply](WrappedReply(_))

          def waiting(requestCount: Int): Behavior[Command] = {
            Behaviors.receiveMessage {
              case WrappedReply(reply) =>
                replyTo ! reply.asInstanceOf[Reply]
                Behaviors.stopped

              case RequestTimeout =>
                sendNextRequest(requestCount + 1)

              case FinalTimeout =>
                replyTo ! timeoutReply
                Behaviors.stopped
            }
          }

          def sendNextRequest(requestCount: Int): Behavior[Command] = {
            if (sendRequest(requestCount, replyAdapter)) {
              timers.startSingleTimer(RequestTimeout, nextRequestAfter)
            } else {
              timers.startSingleTimer(FinalTimeout, finalTimeout)
            }
            waiting(requestCount)
          }

          sendNextRequest(1)
        }
      }
    }
  }
}
