package interactionPattern

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey

/*
  - 추후에 좀 더 볼 것
 */

object RespondingToShardedActor {
  object CounterConsumer {
    sealed trait Command
    final case class NewCount(count: Long) extends Command
    val TypeKey: EntityTypeKey[Command] = EntityTypeKey[Command]("example-sharded-response")
  }

  object Counter {
    trait Command
    case object Increment extends Command
    final case class GetValue(replyToEntityId: String) extends Command
    val TypeKey: EntityTypeKey[Command] = EntityTypeKey[Command]("example-sharded-counter")

    private def apply(): Behavior[Command] =
      Behaviors.setup { context =>
        counter(ClusterSharding(context.system), 0)
      }

    private def counter(sharding: ClusterSharding, value: Long): Behavior[Command] =
      Behaviors.receiveMessage {
        case Increment =>
          counter(sharding, value + 1)

        case GetValue(replyToEntityId) =>
          val replyToEndtityRef = sharding.entityRefFor(CounterConsumer.TypeKey, replyToEntityId)
          replyToEndtityRef ! CounterConsumer.NewCount(value)
          Behaviors.same
      }
  }
}
