package interactionPattern

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, DispatcherSelector}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

/*
  - 응답이 필요 없을 경우 system.ignoreRef 전달하여 request-response => fire-and-forget 으로 전환
  - Use Case
    : 응답이 있는 액터에 메시지를 전송하지만 응답이 필요 없는 경우
  - Problems
    : 실수로 일반적인 ActorRef를 전달할 경우 관계가 깨질 수 있다.
    : ActorSystem 외부에서 Future로 반환되는 ask를 수행할 경우 타임아웃이 발생할 수 있다. ( 종료되지 않으므로 ) ??
    : Terminate 시그널을 받을 수 없다.??
 */

object IgnoringReplies {

  object SayHello {
    case class Request(replyTo: ActorRef[Response])
    case class Response(greet: String)

    def apply(): Behavior[Request] = {
      Behaviors.receiveMessage[Request] {
        case Request(replyTo) =>
          replyTo ! Response("Hello")
          Behaviors.same
      }
    }
  }

  def main(args: Array[String]): Unit = {
    import SayHello._

    val rootBehavior: Behavior[Response] = Behaviors.setup { context =>
      val hi = context.spawn(SayHello(), "hal")

      val hi2 = context.spawn(Behaviors.receiveMessage[Response] {
        case Response(text) =>
          println(text)
          Behaviors.same
      }, name = "hal2")

      // hi ! Request(hi2)
      hi ! Request(context.system.ignoreRef)
      Behaviors.same
    }
    val system = ActorSystem(rootBehavior, "main-app")

    implicit val ec: ExecutionContext = system.dispatchers.lookup(DispatcherSelector.default)
    system.scheduler.scheduleOnce(5.second, () => system.terminate())
  }
}
