package interactionPattern

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, DispatcherSelector, Dispatchers}
import akka.pattern.StatusReply
import akka.util.Timeout

import java.lang.Thread.sleep
import scala.concurrent.{ExecutionContext, TimeoutException}
import scala.concurrent.duration._
import scala.util.Failure
import scala.util.Success

/*
  - 대부분의 경우 응답은 성공이거나 오류이며, 모든 요청에 대해 동일한 상위타입을 갖는 두 개의 응답 클래스 생성이 반복적일 수 있다.
  - 클러스터 컨텍스트에서 메시지가 직렬화되어 네트워크를 통해 전송될 수 있는지 확인해야할 경우에 특히 그렇다.
  - 이를 돕기 위해 일반적인 상태 응답유형인 StatusReply가 존재한다.
 */

object GenericResponseWrapper {
  object Hal {
    sealed trait Command
    case class OpenThePodBayDoorsPlease(replyTo: ActorRef[StatusReply[String]]) extends Command

    def apply(): Behaviors.Receive[Command] =
      Behaviors.receiveMessage[Command] {
        case OpenThePodBayDoorsPlease(replyTo) =>
          // reply with a validation error description
          replyTo ! StatusReply.Error("I'm sorry, Dave, I'm afraid I can't do that.")
          Behaviors.same
      }
  }

  object Dave {
    sealed trait Command

    // 액터 내부에서 사용할 프로토콜의 일부
    private case class AdaptedResponse(message: String) extends Command

    def apply(hal: ActorRef[Hal.Command]): Behavior[Command] =
      Behaviors.setup[Command] { context =>
        // ask 사용을 위해 timeout이 필요하며, timeout이 발생할 경우 TimeoutException 발생한다.
        implicit val timeout: Timeout = 3.seconds

        // A StatusReply.Success(m) ends up as a Success(m) here, while a
        // StatusReply.Error(text) becomes a Failure(ErrorMessage(text))
        context.askWithStatus(hal, Hal.OpenThePodBayDoorsPlease) {
          case Success(message) => AdaptedResponse(message)
          case Failure(StatusReply.ErrorMessage(text)) => AdaptedResponse(s"Request denied: $text")
          case Failure(_) => AdaptedResponse("Request failed")
        }

        Behaviors.receiveMessage {
          // Adapted message(AdptedResponse)는 액터에게 보낸 다른 메시지처럼 처리된다.
          case AdaptedResponse(message) =>
            println(s"Got response from hal : $message")
            Behaviors.same
        }
      }
  }

  def main(args: Array[String]): Unit = {
    val rootBehavior: Behavior[Any] = Behaviors.setup { context =>
      val hal = context.spawn(Hal(), "hal")
      context.spawn(Dave(hal), name = "dave")
      Behaviors.empty
    }
    val system = ActorSystem(rootBehavior, "main-app")
    implicit val ec: ExecutionContext = system.dispatchers.lookup(DispatcherSelector.default)
    system.scheduler.scheduleOnce(5.second, () => system.terminate())
  }
}
