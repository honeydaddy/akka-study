package stash

import scala.concurrent.Future
import scala.util.Failure
import scala.util.Success

import akka.Done
import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, StashBuffer}

/*
  Stashing - 현재 동착을 처리할 수 없거나 처리해서는 안되는 메시지 모두 또는 일부를 버퍼링
  예 - 첫 메시지를 수락하기 전 일부 초기상태 로드 또는 다음 메시지 처리전 완료 대

  - StashBuffer는 메모리에 유지되므로 capacity를 지정해 OutOfMemory를 유의
  - capacity 초과 시 StashOverflowException 발생 - isFull로 방지 가능
  - unstashAll - 순차적으로 처리하며, 완료되기 전 새 메시지에 응답하지 않는다. => 메시지 처리 스레드를 오래 잡아먹을 수 있음을 인지할 것
  =>> 버퍼된 메시지 수를 낮게 유지하는게 좋다..
 */

object Stash {

  trait DB {
    def save(id: String, value: String): Future[Done]
    def load(id: String): Future[String]
  }

  object DataAccess {
    sealed trait Command
    final case class Save(value: String, replyTo: ActorRef[Done]) extends Command
    final case class Get(replyTo: ActorRef[String]) extends Command
    private final case class InitialState(value: String) extends Command
    private case object SaveSuccess extends Command
    private final case class DBError(cause: Throwable) extends Command

    def apply(id: String, db: DB): Behavior[Command] = {
      Behaviors.withStash(100) { buffer =>
        Behaviors.setup[Command] { context =>
          new DataAccess(context, buffer, id, db).start()
        }
      }
    }
  }

  class DataAccess(
    context: ActorContext[DataAccess.Command],
    buffer: StashBuffer[DataAccess.Command],
    id: String,
    db: Stash.DB) {

    import DataAccess._

    private def start(): Behavior[Command] = {
      context.pipeToSelf(db.load(id)) {
        case Success(value) => InitialState(value)
        case Failure(cause) => DBError(cause)
      }

      Behaviors.receiveMessage {
        case InitialState(value) =>
          buffer.unstashAll(active(value))
        case DBError(cause) =>
          throw cause
        case other =>
          buffer.stash(other)
          Behaviors.same
      }
    }

    private def active(state: String): Behavior[Command] = {
      Behaviors.receiveMessagePartial {
        case Get(replyTo) =>
          replyTo ! state
          Behaviors.same
        case Save(value, replyTo) =>
          context.pipeToSelf(db.save(id, value)) {
            case Success(_) => SaveSuccess
            case Failure(cause) => DBError(cause)
          }
          saving(value, replyTo)
      }
    }

    private def saving(state: String, replyTo: ActorRef[Done]): Behavior[Command] ={
      Behaviors.receiveMessage {
        case SaveSuccess =>
          replyTo ! Done
          buffer.unstashAll(active(state))
        case DBError(cause) =>
          throw cause
        case other =>
          buffer.stash(other)
          Behaviors.same
      }
    }
  }
}
