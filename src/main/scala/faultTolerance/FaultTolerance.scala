package faultTolerance

import akka.actor.DeathPactException
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, ChildFailed, DispatcherSelector, PostStop, PreRestart, SupervisorStrategy, Terminated}
import akka.actor.typed.scaladsl.Behaviors
import akka.util.Timeout
import akka.actor.typed.scaladsl.AskPattern._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt

/*
  validate error - 프로토콜의 일부로 정의
  failure - 프로토콜의 일부로 모델링하는 것은 유용하지 않다.
  대부분 해결 방법은 새로운 상태의 새 액터를 시작
 */

object FaultTolerance {

  val behavior = Behaviors.empty[String]

  // 일반적으로 액터를 자식으로 스폰 할 때 부모에서 Supervisor로 자식 액터를 감싼다.
  Behaviors.supervise(behavior).onFailure[IllegalStateException](SupervisorStrategy.restart)

  // Wrapping
  object Counter {
    sealed trait Command
    case class Increment(nr: Int) extends Command
    case class GetCount(replyTo: ActorRef[Int]) extends Command

    def apply(): Behavior[Command] = {
      // 최상위 수준에 Supervisor 추가
      Behaviors.supervise(counter(1)).onFailure(SupervisorStrategy.restart)
    }

    private def counter(count: Int): Behavior[Command] =
      Behaviors.receiveMessage[Command] {
        case Increment(nr: Int) =>
          counter(count + nr)
        case GetCount(replyTo) =>
          replyTo ! count
          Behaviors.same
      }
  }

  // 부모 액터 재시작될 경우 자식액터는 중지된다. ( 자식 액터를 생성하는 리소스 누출 방지 )
  def child(size: Long): Behavior[String] =
    Behaviors.receiveMessage(msg => child(size + msg.length))

  def parent: Behavior[String] = {
    Behaviors
      .supervise[String] {
        Behaviors.setup { context =>
          // 자식 액터의 시작이 부모 액터가 다시 시작되는 setup 블록에 존재하며, 다시 시작도리 때 자식 액터는 중지된다.
          val child1 = context.spawn(child(0), "child1")
          val child2 = context.spawn(child(0), "child2")

          Behaviors.receiveMessage[String] { msg =>
            val parts = msg.split(" ")
            child1 ! parts(0)
            child2 ! parts(1)
            Behaviors.same
          }
        }
      }.onFailure(SupervisorStrategy.restart)
  }

  // 부모 액터가 재시작 되어도 자식액터가 영향을 받지 않기 위해서는 supervise가 setup 내부에 존재해야 하며, withStopChildren(false) 필요
  def parnet2: Behavior[String] = {
    Behaviors.setup { context =>
      val child1 = context.spawn(child(0), "child1")
      val child2 = context.spawn(child(0), "child2")

      // supervision strategy inside the setup to not recreate children on restart
      Behaviors
        .supervise {
          Behaviors.receiveMessage[String] { msg =>
            val parts = msg.split(" ")
            child1 ! parts(0)
            child2 ! parts(1)
            Behaviors.same
          }
        }.onFailure(SupervisorStrategy.restart)
    }
  }

  // PreRestart 신호
  // 감독 중인 액터가 다시 시작되기 전 리소스를 정리할 수 잇는 기회를 제공하는 PreRestart 신호가 전송된다.
  // PostStop는 restart에 생기지 않기때문에 일반적으로 PreRestart와 PostStop 둘 다 리소스 정리를 처리해야 한다.
  trait Resource {
    def close(): Unit
    def process(parts: Array[String]): Unit
  }
  def claimResouce(): Resource = ???

  def withPreRestart: Behavior[String] = {
    Behaviors
      .supervise[String] {
        Behaviors.setup { context =>
          val resource = claimResouce()

          Behaviors
            .receiveMessage[String] { msg =>
              val parts = msg.split(" ")
              resource.process(parts)
              Behaviors.same
            }
            .receiveSignal {
              case (_, signal) if signal == PreRestart || signal == PostStop =>
                resource.close()
                Behaviors.same
            }
        }
      }.onFailure[Exception](SupervisorStrategy.restart)
  }

  // 계층구조를 통한 실패 버블링(?)
  // 일부 시나이로에서는 Actor 계층 구조에서 장애에 대한 처리를 상위 Actor 에서 처리하는 것이 유용할 수 있다.
  // 자녀가 중지되었을 때 알림을 받기위해서는 watch 필요하다.
  // 자식이 실패로 인해 중지된 경우 원인을 포함한 신호가 수신된다.
  // ChildFailed는 Terminated를 확장한 것이며, 중지/실패를 구분할 필요가 없을 경우 Terminateㅇ로 처리 가능
  // 자식이 실패를 일으킨 원래 예외는 직계 부모만 사용할 수 있다.
  object Protocol {
    sealed trait Command
    case class Fail(text: String) extends Command
    case class Hello(text: String, replyTo: ActorRef[String]) extends Command
  }

  object Worker {
    import Protocol._
    def apply(): Behavior[Command] =
      Behaviors.receiveMessage {
        case Fail(text) =>
          throw new RuntimeException(text)
        case Hello(text, replyTo) =>
          println(text)
          replyTo ! text
          Behaviors.same
      }
  }

  object MiddleManagement {
    import Protocol._
    def apply(): Behavior[Command] =
      Behaviors.setup[Command] { context =>
        println("Middle management starting up")
        // default supervision of child, meaning that it will stop on failure
        val child = context.spawn(Worker(), "child")
        // we want to know when the child terminates. but since we do not handle
        // the Terminated signal, we will in turn fail on child termination
        context.watch(child)

        // here we don't handle Terminated at all which means that
        // when the child fails or stops gracefully this actor will
        // fail with a DeathPactException
        Behaviors.receiveMessage { message =>
          child ! message
          Behaviors.same
        }
      }
  }

  object Boss {
    import Protocol._
    def apply(): Behavior[Command] =
      Behaviors
        .supervise(Behaviors.setup[Command] { context =>
          println("Boss starting up")
          // default supervision of child, meaning that it will stop on failure
          val middleManagement = context.spawn(MiddleManagement(), "middle-management")
          context.watch(middleManagement)

          // here we don't handle Terminated at all which means that
          // when the child fails or stops gracefully this actor will
          // fail with a DeathPactException
          Behaviors
            .receiveMessage[Command] { message =>
              middleManagement ! message
              Behaviors.same
            }
            /*
            .receiveSignal {
              case (_, ChildFailed(e)) =>
                println(e)
                Behaviors.same
            }
            */
        }).onFailure[DeathPactException](SupervisorStrategy.restart)
  }

  def main(args: Array[String]) {
    implicit val timeout: Timeout = 1.seconds
    implicit val system = ActorSystem(Boss(), "Boss")
    implicit val ec: ExecutionContext = system.dispatchers.lookup(DispatcherSelector.default)
    system.scheduler.scheduleOnce(5.seconds, () => system.terminate())

    import Protocol._

    system.ask(ref => Hello("hi", ref))
    system ! Fail("bye")
    system.ask(ref => Hello("hi2", ref))

  }
}
