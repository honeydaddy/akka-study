name := "akkaStudy"

version := "0.1"

scalaVersion := "2.12.13"

val akkaVersion = "2.6.13"

libraryDependencies += "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion
libraryDependencies += "com.typesafe.akka" %% "akka-cluster-sharding-typed" % akkaVersion
